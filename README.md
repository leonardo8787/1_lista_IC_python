<h1>1_lista_IC_python</h1>
lista de exercícios e teste de software do exercício de conversão das horas!

<h2>Objetivos</h2>
1.determinar os valores de entrada/saída esperada usando os critérios de particionamento e análise de valor limite.<br>
2.codificar os casos de teste usando o unittest.<br>

<h2>Critério de particionamento</h2>

O critério de particionamento de um caso de teste trata-se de particionar as entradas do software a fim de extrair saidas esperadas.<br>


<table>
    <tr><td>Entradas</td><td>Saídas</td></tr>
    <tr><td>0</td><td>0</td></tr>
    <tr><td>1</td><td>1</td></tr>
    <tr><td>2</td><td>2</td></tr>
    <tr><td>3</td><td>3</td></tr>
    <tr><td>4</td><td>4</td></tr>
    <tr><td>5</td><td>5</td></tr>
    <tr><td>6</td><td>6</td></tr>
    <tr><td>7</td><td>7</td></tr>
    <tr><td>8</td><td>8</td></tr>
    <tr><td>9</td><td>9</td></tr>
    <tr><td>10</td><td>10</td></tr>
    <tr><td>11</td><td>11</td></tr>
    <tr><td>12</td><td>12</td></tr>
    <tr><td>13</td><td>1</td></tr>
    <tr><td>14</td><td>2</td></tr>
    <tr><td>15</td><td>3</td></tr>
    <tr><td>16</td><td>4</td></tr>
    <tr><td>17</td><td>5</td></tr>
    <tr><td>18</td><td>6</td></tr>
    <tr><td>19</td><td>7</td></tr>
    <tr><td>20</td><td>8</td></tr>
    <tr><td>21</td><td>9</td></tr>
    <tr><td>22</td><td>10</td></tr>
    <tr><td>23</td><td>11</td></tr>
    <tr><td>24</td><td>12</td></tr>
</table>


<h2>exemplo de código:</h2>


~~~

    #teste: critério de particionamento
    def test_entradas_1(self):
        entradas = (0,1,2,3,4,5,6,7,8,9,10,11,12)
        entrada_2 = 12
        for entrada in entradas:
            with self.subTest(entrada=entrada, entrada_2=entrada_2):
                hora, minuto = converter_hora(entrada, entrada_2)
                self.assertEqual(
                    #converter_hora(entrada),
                    hora,
                    entrada,
                    msg=f'"{entrada}" não retornou "{entrada}"'
                )
                self.assertEqual(
                    #converter_hora(entrada),
                    minuto,
                    entrada_2,
                    msg=f'"{entrada_2}" não retornou 12'
                )

    def test_entradas_2(self):
        entradas = (24,13,14,15,16,17,18,19,20,21,22,23)
        saida= -1
        entrada_2 = 12
        for entrada in entradas:
            saida+=1
            with self.subTest(entrada=entrada,saida=saida, entrada_2=entrada_2):
                hora, minuto = converter_hora(entrada, entrada_2)
                self.assertEqual(
                    hora,
                    saida,
                    msg=f'"{entrada}" não retornou "{saida}"'
                )
                self.assertEqual(
                    minuto,
                    entrada_2,
                    msg=f'"{entrada}" não retornou "{entrada_2}"'
                )
~~~


<h2>Análise de valor limite</h2>
Este critério é complementar ao Particionamento de Equivalência e avalia os valores limites de
cada intervalo estabelecido sobre o domínio de entrada. Desta forma, testar usando este critério
significa testar o valor mínimo e o antecessor imediato (ou o valor máximo e o imediatamente
subseqüente) nesses intervalos. Segundo Pressman (2002), os erros geralmente ocorrem nos limi-
tes dos domínios de entrada. Desta forma, pode se dizer que a aplicação desse critério serve para
verificar a forma como funções do programa que funcionam adequadamente para um valor inter-
mediário de um intervalo, funcionariam para valores limites no mesmo. Um exemplo típico de erro
detectado por esse critério é aquele relacionado a comandos de iteração, onde o programador pode
cometer enganos no tratamento dos valores limites, gerando defeitos que ocasionem em erros.

<h2>exemplo:</h2>


~~~~~

    #Teste do valor limite
    def teste_saidas_1(self):
        horas = (23,24,25,26,27,30,60)
        saida = 24
        minuto = 12
        for hora in horas:
            if hora > saida:
                with self.subTest(hora=hora, saida=saida):
                    hora, minuto = converter_hora(hora, minuto)
                    if hora > saida:
                        self.assertEqual(
                            hora,
                            saida,
                            msg=f'não existe hora acima de 24h'
                        )

    def teste_saidas_2(self):
        horas = (1,0,-1,-2,-3,-4)
        saida = 0
        minuto = 12
        for hora in horas:
            if hora < saida:
                with self.subTest(hora=horas, saida=saida):
                    hora, minuto = converter_hora(hora, minuto)
                    self.assertEqual(
                        hora,
                        saida,
                        msg=f'não existe hora abaixo de 0h'
                    )
~~~~~
