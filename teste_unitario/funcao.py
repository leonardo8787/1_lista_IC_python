#função de conversão do horário
def converter_hora(hora, minuto):
	if hora > 12 and hora != 24:
		contador = 0
		for x in range(hora):
			if x >= 12:
				contador += 1
		return contador, minuto
	elif hora == 24:
		return 0, minuto
	else:
		return hora, minuto
