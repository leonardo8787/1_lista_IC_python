"""
Exercício de conversão de horas
IC - teste de software
Leonardo Campos
CEFET/MG
"""

from funcao import converter_hora


class HoraInvalida(Exception):
    pass

""" 
#função de conversão do horário
def converter_hora(hora, minuto):
	if hora > 12 and hora != 24:
		contador = 0
		for x in range(hora):
			if x >= 12:
				contador += 1
		return contador, minuto
	elif hora == 24:
		return 0, minuto
	else:
		return hora, minuto
"""

#verificador
verifica = 's'

while verifica != 'n':
	hora = int(input("Digite a hora: "))
	minuto = int(input("Digite os minutos: "))
	#if (hora < 0) or (minuto < 0):
	#	raise HoraInvalida()
	if hora < 12:
		print(converter_hora(hora, minuto), end=":")
		print(minuto, end=" ")
		print("AM")
	else:
		print(converter_hora(hora, minuto), end=":")
		print(minuto, end=" ")
		print("PM")
	verifica = input("Continuar? s(sim)/n(nao): ")
	if verifica == 'n':
	    print('fim...')
