import unittest
from funcao import converter_hora

class TesteHorario(unittest.TestCase):

    #teste: critério de particionamento
    def test_entradas_1(self):
        entradas = (0,1,2,3,4,5,6,7,8,9,10,11,12)
        entrada_2 = 12
        for entrada in entradas:
            with self.subTest(entrada=entrada, entrada_2=entrada_2):
                hora, minuto = converter_hora(entrada, entrada_2)
                self.assertEqual(
                    #converter_hora(entrada),
                    hora,
                    entrada,
                    msg=f'"{entrada}" não retornou "{entrada}"'
                )
                self.assertEqual(
                    #converter_hora(entrada),
                    minuto,
                    entrada_2,
                    msg=f'"{entrada_2}" não retornou 12'
                )

    def test_entradas_2(self):
        entradas = (24,13,14,15,16,17,18,19,20,21,22,23)
        saida= -1
        entrada_2 = 12
        for entrada in entradas:
            saida+=1
            with self.subTest(entrada=entrada,saida=saida, entrada_2=entrada_2):
                hora, minuto = converter_hora(entrada, entrada_2)
                self.assertEqual(
                    hora,
                    saida,
                    msg=f'"{entrada}" não retornou "{saida}"'
                )
                self.assertEqual(
                    minuto,
                    entrada_2,
                    msg=f'"{entrada}" não retornou "{entrada_2}"'
                )

    #teste com entradas específicas
    def teste_teste(self):
        hora,minuto = converter_hora(9,12)
        self.assertEqual(hora, 9)
        self.assertEqual(minuto, 12)


    #Teste do valor limite
    def teste_saidas_1(self):
        horas = (23,24,25,26,27,30,60)
        saida = 24
        minuto = 12
        for hora in horas:
            if hora > saida:
                with self.subTest(hora=hora, saida=saida):
                    hora, minuto = converter_hora(hora, minuto)
                    if hora > saida:
                        self.assertEqual(
                            hora,
                            saida,
                            msg=f'não existe hora acima de 24h'
                        )

    def teste_saidas_2(self):
        horas = (1,0,-1,-2,-3,-4)
        saida = 0
        minuto = 12
        for hora in horas:
            if hora < saida:
                with self.subTest(hora=horas, saida=saida):
                    hora, minuto = converter_hora(hora, minuto)
                    self.assertEqual(
                        hora,
                        saida,
                        msg=f'não existe hora abaixo de 0h'
                    )


unittest.main(verbosity=2)